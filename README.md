# flask-API-Chatbot 

#### Implemetação de Apí Flask para Chatbot(https://gitlab.com/ricarducorrea/bot-api) using Flask.

## Local Setup:
 1. Ensure that Python, Flask, sqlite3, and ChatterBot are installed (either manually, or run `pip install -r requirements.txt`).
 2. Run *app.py* with `python app.py`.
 3. The demo will be live at [http://localhost:5000/](http://localhost:5000/)

 

## informações técnicas:
1. Arquitetura em Camadas
2. Teste Unitário
3. Api Python em Flask
4. Banco de dados SQLite

## Melhorias Futuras
1. Implementar o chatbot utilizando biblioteca chatterbot
2. Gerenciar mensagens utilizando RabbitMQ
3. Armazenar  mensagens no ElasticSearch para uma melhor perfomance de letura e scrita das mensagens
4. Aplicar Machine Learning para uma maior acertividade no entendimento do contexto da mensagem e uma resposta mais pontual do Bot.
5. Subir os serviços em docker utiliando uma stack.

 

## License
This source is free to use, but ChatterBot does have a license which still applies and can be found on the [LICENSE](https://github.com/gunthercox/ChatterBot/blob/master/LICENSE) page.
