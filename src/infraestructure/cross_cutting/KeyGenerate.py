import random
import string
 
class KeyGenerate:
    @staticmethod 
    def get_key(string_format):
        variacoes=string_format.split("-")
        key=[]
        for i in variacoes:
            key.append("".join(random.choice(string.ascii_lowercase + string.digits) for _ in range(int(i)))    )
        return "-".join(key)
    
 