from src.infraestructure.cross_cutting.KeyGenerate import KeyGenerate
from src.domain.Bot import Bot
from src.infraestructure.repositories.BotRepository import BotRepository
class BotService:
    def __init__(self):
        self.bot_repository=BotRepository()
        
 
    def create(self,name)->Bot:
        bot=Bot()
        if(name):
            bot=Bot(name, KeyGenerate.get_key("8-4-4-4-12"))
            self.bot_repository.insert_bot(bot)
        return bot
    
    def update(self,id, name):   
        bot= self.get(id)   
        if(bot and id and name):
            bot.name=name
            self.bot_repository.update_bot_by_id(bot)
            return True
        return False
        
    def get(self,id):
        if(id):
            return self.bot_repository.get_bot_id(id) 
    
    def delete(self,id):
        if(self.get(id)):
            return self.bot_repository.deleta_bot_by_id(id) 