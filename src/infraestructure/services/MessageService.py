from src.infraestructure.cross_cutting.KeyGenerate import KeyGenerate
from src.infraestructure.cross_cutting.DateTimeExtensions import DateTimeExtensions
from src.infraestructure.repositories.MessageRepository import MessageRepository
from src.domain.Message import Message
class MessageService:
    def __init__(self):        
        self.message_repository=MessageRepository()
 
    def create(self,conversationId,timestamp,from_,to,text):
        try:
            id=KeyGenerate.get_key("8-4-4-4-12")
            #timestamp=DateTimeExtensions.get_date_str()
            if(id and conversationId and from_ and to and text and timestamp):
                message=Message(id,conversationId,timestamp,from_,to,text)
                return self.message_repository.insert_message(message)
        except Exception as ex:
            msg = ("%s - %s - %s (%s)" %(sys.exc_info()[0], sys.exc_info()[1], os.getcwd(), ex))
            print("ERROR <<MessageService.create>>:",msg,"CREATE MESSAGE ERROR") 
    
   
            
    def getMessageId(self,id):
        if(id):
            return self.message_repository.get_message_id(id) 
    
    def getConversationId(self,conversationId):
        if(id):
            return self.message_repository.get_converasation_id(conversationId) 
    