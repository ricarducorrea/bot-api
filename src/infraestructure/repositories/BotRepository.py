 
from src.domain.SQLite import SQLite
from src.domain.Bot import Bot
from src.infraestructure.cross_cutting.KeyGenerate import KeyGenerate
from time import sleep
import sys
import os
class BotRepository(SQLite):
    def __init__(self):
        super().__init__()
        
    def get_bot_id(self,id) -> Bot:  
        try:
            bot=Bot()
            result_select = self.select("SELECT * FROM  BOT where ID='{0}'".format(id)) 
            for row in result_select:                   
                bot.id = row[0]
                bot.name = row[1]                 
            return bot
        except Exception as ex:
            msg = ("%s - %s - %s (%s)" %(sys.exc_info()[0], sys.exc_info()[1], os.getcwd(), ex))
            print("ERROR <<BotRepository.BotRepository>>:",msg)                  
        finally:
            self.close()
    
    
    def deleta_bot_by_id(self, id) -> bool:
        try:
           
            sql = f""" DELETE FROM  BOT WHERE ID ='%s' """%(id)
            self.set_by_query(sql)
            return True
        except Exception as ex:
            msg = ("%s - %s - %s (%s)" %(sys.exc_info()[0], sys.exc_info()[1], os.getcwd(), ex))
            print("ERROR <<BotRepository.deleta_bot_by_id>>:",msg)
            
    def insert_bot(self,bot:Bot) -> bool:   
        try:           
            sql = """  INSERT INTO  BOT ( ID, NAME) VALUES ('{0}','{1}' ) """.format(bot.id, bot.name)
            self.set_by_query(sql)
            return True         
        except Exception as ex:
            msg = ("%s - %s - %s (%s)" %(sys.exc_info()[0], sys.exc_info()[1], os.getcwd(), ex))
            print("ERROR <<BotRepository.insert_bot>>:",msg)                   
        
            
            
    def update_bot_by_id(self,bot:Bot) -> bool:   
        try:             
            sql = """  UPDATE  BOT SET NAME='{1}' WHERE ID='{0}' """.format(bot.id, bot.name)
            self.set_by_query(sql)
            return True         
        except Exception as ex:
            msg = ("%s - %s - %s (%s)" %(sys.exc_info()[0], sys.exc_info()[1], os.getcwd(), ex))
            print("ERROR <<BotRepository.insert_bot>>:",msg)    
               
      
             
            
        
 