 
from src.domain.SQLite import SQLite
from src.domain.Message import Message
 
from time import sleep
import sys
import os
class MessageRepository(SQLite):
    def __init__(self):
        super().__init__()
    
    def get_message_id(self,id) -> Message:  
        try:
            message=Message()
            result_select = self.select("SELECT * FROM  MESSAGE where ID='{0}'".format(id)) 
            for row in result_select:               
                message.id = row[0]
                message.conversationId = row[1]
                message.timestamp = row[2]
                message.from_ = row[3]
                message.to = row[4]
                message.text = row[5]              
            return message
        except Exception as ex:
            msg = ("%s - %s - %s (%s)" %(sys.exc_info()[0], sys.exc_info()[1], os.getcwd(), ex))
            print("ERROR <<MessageRepository.get_message_id>>:",msg)                  
        finally:
            self.close()
    
    def get_converasation_id(self,converasationId):  
        try:
            messages=[]
            result_select = self.select("SELECT * FROM  MESSAGE where CONVERSATIONID='{0}'".format(converasationId)) 
            for row in result_select:                    
                message=Message()
                message.id = row[0]
                message.conversationId = row[1]
                message.timestamp = row[2]
                message.from_ = row[3]
                message.to = row[4]
                message.text = row[5]   
                messages.append(message)           
            return messages
        except Exception as ex:
            msg = ("%s - %s - %s (%s)" %(sys.exc_info()[0], sys.exc_info()[1], os.getcwd(), ex))
            print("ERROR <<MessageRepository.get_message_id>>:",msg)                  
        finally:
            self.close()
    
     
            
    def insert_message(self,message:Message) -> bool:   
        try:
           
            sql = """INSERT INTO  MESSAGE ( ID, CONVERSATIONID, TIMESTAMP_, FROM_, TO_, TEXT_) 
            VALUES ('{0}','{1}','{2}','{3}','{4}','{5}') """.format( 
                                                            message.id,
                                                            message.conversationId,
                                                            message.timestamp,
                                                            message.from_,
                                                            message.to,
                                                            message.text )
            self.set_by_query(sql)
            return message         
        except Exception as ex:
            msg = ("%s - %s - %s (%s)" %(sys.exc_info()[0], sys.exc_info()[1], os.getcwd(), ex))
            print("ERROR <<MessageRepository.insert_message>>:",msg)    
               
        finally:
            self.close()
           
             
            
        
 