#!/usr/bin/python
import os
import sys 
import sqlite3

class SQLite:
    def __init__(self):           
        self.connect()
        self.install()    
    
    def connect(self):
        self.conn=sqlite3.connect('vivo_challeng.db')   
    
    def set_by_query(self, query):
        try: 
            self.connect()
            self.conn.execute(query)    
            self.conn.commit()       
        except Exception as ex:
            msg = ("%s - %s - %s (%s)" %(sys.exc_info()[0], sys.exc_info()[1], os.getcwd(), ex))
            print("ERROR <<SQLite.set_by_query>>:",msg)                    
        finally:
            self.close()
            pass
    
    def select(self,query):
        try:  
            self.connect()
            result_select=self.conn.execute(query)      
               
            return self.conn.execute(query)      
        except Exception as ex:
            msg = ("%s - %s - %s (%s)" %(sys.exc_info()[0], sys.exc_info()[1], os.getcwd(), ex))
            print("ERROR <<SQLite.select>>:",msg)  
        #finally: 
            #self.close() 
    
    def deleta(self, query):
        try:     
            self.connect()          
            self.conn.execute(query)   
            self.conn.commit()          
        except Exception as ex:
            msg = ("%s - %s - %s (%s)" %(sys.exc_info()[0], sys.exc_info()[1], os.getcwd(), ex))
            print("ERROR <<SQLite.deleta>>:",msg)                
        finally:                       
            self.close()
                
    def close(self):        
        self.conn.close()
        
    def install(self):
        self.conn.execute(
            "CREATE TABLE IF NOT EXISTS BOT(ID TEXT PRIMARY KEY, NAME TEXT NOT NULL);")
        
        self.conn.execute(
            '''CREATE TABLE IF NOT EXISTS MESSAGE(
            ID TEXT PRIMARY KEY NOT NULL,
            CONVERSATIONID TEXT NOT NULL, 
            TIMESTAMP_ NUMERIC NOT NULL,
            FROM_ TEXT NOT NULL,
            TO_ TEXT NOT NULL,
            TEXT_ TEXT NOT NULL
            );''')
        
     
    