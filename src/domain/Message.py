class Message:
    def __init__(self,id=None,conversationId=None,timestamp=None,from_=None,to=None,text=None):
        self.__conversationId = conversationId
        self.__timestamp = timestamp
        self.__from_ = from_
        self.__to = to
        self.__text = text
        self.__id = id
      
         
         
    @property
    def id(self):
        return self.__id

    @id.setter
    def id(self, id):
        self.__id = id
    
    @property
    def conversationId(self):
        return self.__conversationId

    @conversationId.setter
    def conversationId(self, conversationId):
        self.__conversationId = conversationId
        
    @property
    def timestamp(self):
        return self.__timestamp

    @timestamp.setter
    def timestamp(self, timestamp):
        self.__timestamp = timestamp
        
    @property
    def from_(self):
        return self.__from_

    @from_.setter
    def from_(self, from_):
        self.__from_ = from_
        
    @property
    def to(self):
        return self.__to

    @to.setter
    def to(self, to):
        self.__to = to
        
    @property
    def text(self):
        return self.__text

    @text.setter
    def text(self, text):
        self.__text = text
        
     
     
        
    