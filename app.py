#!/usr/bin/python
from flask import Flask, render_template,  jsonify, request
import json
from src.infraestructure.services.BotService import BotService
from src.infraestructure.services.MessageService import MessageService
app = Flask(__name__)


@app.route("/bots", methods=['PUT'])
def put_bot():
    content = request.get_json()
    bot_name=content.get('name')
    bot=BotService().create(bot_name)
    return jsonify(  {"id":bot.id,"name":bot.name}),200
     

@app.route("/bots/<string:id>", methods=['GET'])
def get_bot(id):
    bot=None
    content = request.get_json()
    if request.method == 'GET':
        bot=BotService().get(id)
    return jsonify( {"id":bot.id,"name":bot.name}),200
    
  
    
@app.route("/bots", methods=['POST'])
def post_bot():
    bot=None
    content = request.get_json()
    if request.method == 'POST':
        if(BotService().get(id)):
            BotService().update(content.get('id'), content.get('name'))
    return jsonify( {}),200
        
@app.route("/bots/<string:id>", methods=['GET', 'POST', 'DELETE'])
def delete_bot(id):
    bot=None
    content = request.get_json()
    if request.method == 'DELETE':
        if(BotService().delete(id)):
            return jsonify( {}),200
    
     

@app.route("/messages", methods=['POST'])
def post_mesaage():
    content = request.get_json()       
    conversationId=content.get('conversationId')
    timestamp=content.get('timestamp')
    from_=content.get('from')
    to=content.get('to')
    text=content.get('text')
    if( conversationId and timestamp and from_ and to and text):
        MessageService().create(conversationId , timestamp , from_ , to , text)
    return jsonify( {}),200
        
     
     

@app.route("/messages/<string:id>/", methods=['GET'])
def get_mesaage_id(id):
    if(id):
        MessageService().create
        
@app.route("/messages")
def messages():
    conversationId = request.args.get('conversationId', default = None, type = str) 
    if(conversationId):
        messages=MessageService().getConversationId(conversationId)
        json_string = json.dumps([ob.__dict__ for ob in messages])
        json_string =json_string.replace("_Message__", "")
        json_string =json_string.replace("_", "")
        return jsonify( json.loads(json_string)),200
    


if __name__ == "__main__":
    app.run()
