import unittest
from src.infraestructure.services.BotService import BotService
from src.infraestructure.services.MessageService import MessageService
from src.domain.Bot import Bot
from src.domain.Message import Message


class TestBotService(unittest.TestCase):

    def test_add_bot(self):         
        self.assertEqual(type(BotService().create("T1")), (Bot))
    
    def test_get_bot(self):  
        bot=BotService().create("T3")          
        self.assertEqual(type(BotService().get(bot.id)), (Bot))
    
    def test_update_bot(self): 
        bot=BotService().create("T4")           
        self.assertEqual(BotService().update(bot.id,"NOVO BOT"), True)
    
    def test_delete_bot(self): 
        bot=BotService().create("T2")        
        self.assertEqual(BotService().delete(bot.id), True)


class TestMessageService(unittest.TestCase):
    
    def test_add_message(self):         
        self.assertEqual(type(MessageService().create("01","timestamp","from_","to","text")), (Message))
    
    def test_get_messageId(self):  
        message=MessageService().create("02","timestamp","from_","to","text")      
        cId=MessageService().getMessageId(message.id)
        self.assertEqual(MessageService().getMessageId(message.id).id, message.id)
    
    def test_get_conversationId(self):  
        message=MessageService().create("02","timestamp","from_","to","text")          
        self.assertEqual(type(MessageService().getConversationId(message.conversationId)), list)
    
    
         

if __name__ == '__main__':
    unittest.main()